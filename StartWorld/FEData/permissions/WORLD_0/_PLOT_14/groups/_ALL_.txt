#Permissions for group _ALL_
#DO NOT MODIFY OR REMOVE fe.internal PERMISSIONS UNLESS YOU KNOW WHAT YOU DO!
#After you modified permissions in this file, remember to directly run "/feperm reload", or the changes get overwritten next time permissions are saved by the server.
#Thu Jan 14 09:49:06 CST 2016
fe.internal.plot.owner=4e345c9c-c520-4f3d-8c55-46b8d7c907ec
fe.internal.zone.hidden=true
fe.protection.break.*=false
fe.protection.interact.*=false
fe.protection.place.*=false
fe.protection.use.*=false
