#!/bin/bash

# BlockHeads.ML X64 BSD Based Startup script. Checks for PIDs and launches into a screen session for owner user only.

# http://www.blockheads.ml

# Copyright BlockHeads.ML see, LICENSE file for details.

cd /minecraft;

autoStart="true"

if ! screen -list | grep -q "minecraft" && "$autoStart" -eq "true"; then
	cd /minecraft;
	echo "Starting Minecraft server..."
	screen -L -dmS minecraft /usr/bin/java -Xmx12G -Xms2G -jar forge-1.7.10-10.13.4.1558-1.7.10-universal.jar nogui
	echo "Started Minecraft server..."
else
	echo "Minecraft server already started or $autoStart set to false..."
fi
